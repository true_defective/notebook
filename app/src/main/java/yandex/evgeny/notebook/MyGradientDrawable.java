package yandex.evgeny.notebook;

import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;

public class MyGradientDrawable extends GradientDrawable {
    private int savedColor;

    public int GetSavedColor() {
        return savedColor;
    }

    @Override
    public void setColor(int argb) {
        savedColor = argb;
        super.setColor(argb);
    }
}
