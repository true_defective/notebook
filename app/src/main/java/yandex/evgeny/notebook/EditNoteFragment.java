package yandex.evgeny.notebook;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;

public class EditNoteFragment extends Fragment implements IBackButtonListener {

    public static final String ADD_REQUEST_TITLE = "Adding note";
    public static final String EDIT_REQUEST_TITLE = "Editing note";
    public static final String EXIT_WITHOUT_SAVING_TITLE = "Exit without saving";
    public static final String EXIT_WITHOUT_SAVING_MESSAGE = "Are you sure you want to leave without saving?";
    public static final String EXIT_WITHOUT_SAVING_YES = "Yes";
    public static final String EXIT_WITHOUT_SAVING_NO = "Cancel";
    public static final String INTENT_COLOR_KEY = "color";
    public static final String ERROR_NOHEADER_TEXT = "Title required to save note!";
    public static final String SAVED_STATE_NOTE_KEY = "note";
    public static final String SAVED_STATE_POSITION_KEY = "color";
    public static final String SAVED_STATE_COLOR_KEY = "color";
    public static final int RESULT_OK = 1;

    public static final int CHOOSE_COLOR_REQUEST = 0;

    private static ArrayList<String> colors =
            new ArrayList<>(Arrays.asList("#b39ddb", "#b2ebf2", "#81d4fa", "#e6ee9c", "#ffd54f", "#b0bec5"));

    private EditText noteHeader;
    private EditText noteText;
    public int noteColor;

    private IFragmentsHolder fragmentsHolder;
    private Note oldNote;
    private boolean isNeedToSave;
    private int notePosition = 0;

    public static EditNoteFragment newInstance(@Nullable Note note, int notePosition) {
        Bundle args = new Bundle();

        EditNoteFragment fragment = new EditNoteFragment();
        args.putSerializable(SAVED_STATE_NOTE_KEY, note);
        args.putInt(SAVED_STATE_POSITION_KEY, notePosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oldNote = (Note) getArguments().getSerializable(SAVED_STATE_NOTE_KEY);
        notePosition = getArguments().getInt(SAVED_STATE_POSITION_KEY);
        noteColor = getDefaultColor();
        if (oldNote != null) {
            noteColor = oldNote.getColor();
        }
        if (savedInstanceState != null) {
            noteColor = savedInstanceState.getInt(SAVED_STATE_COLOR_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.note_editing_fragment, container, false);
        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(ADD_REQUEST_TITLE);

        rootView.findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote(view);
            }
        });
        noteHeader = rootView.findViewById(R.id.noteHeader);
        noteText = rootView.findViewById(R.id.noteText);

        if (oldNote != null)
        {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(EDIT_REQUEST_TITLE);
            noteHeader.setText(oldNote.getHeader());
            noteText.setText(oldNote.getText());
        }

        return rootView;
    }

    public void saveNote(View view)
    {
        isNeedToSave = true;
        finish();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.edit_menu, menu);

        MenuItem colorChooser = menu.findItem(R.id.color_chooser);
        colorChooser.getIcon().setColorFilter(getResources().getColor(R.color.colorTextIcons), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.color_chooser:
                ColorsEditorFragment fragment = ColorsEditorFragment.newInstance(noteColor);
                fragment.setTargetFragment(this, CHOOSE_COLOR_REQUEST);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
        }

        return super.onOptionsItemSelected(item);
    }

    public void finish() {
        Intent data = new Intent();
        if (isNeedToSave) {
            String headerText = noteHeader.getText().toString().trim();
            String noteDecription = noteText.getText().toString();
            if (headerText != null && !headerText.isEmpty()) {
                Note note = new Note(headerText, noteDecription, noteColor);
                data.putExtra(NotesListFragment.INTENT_NOTE_KEY, note);
                data.putExtra(NotesListFragment.INTENT_POSITION_KEY, notePosition);
                Fragment targetFragment = getTargetFragment();
                targetFragment.onActivityResult(getTargetRequestCode(), NotesListFragment.RESULT_OK, data);
            } else {
                noteHeader.requestFocus();
                noteHeader.setError(ERROR_NOHEADER_TEXT);
                isNeedToSave = false;
                return;
            }
        }

        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IFragmentsHolder) {
            fragmentsHolder = (IFragmentsHolder) context;
        } else {
            throw new IllegalStateException("Context must implement IFragmentsHolder");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE_COLOR_KEY, noteColor);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CHOOSE_COLOR_REQUEST) {
                if (data != null && data.hasExtra(INTENT_COLOR_KEY)) {
                    noteColor = data.getIntExtra(INTENT_COLOR_KEY, noteColor);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isNoteChanged()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(EXIT_WITHOUT_SAVING_TITLE)
                    .setMessage(EXIT_WITHOUT_SAVING_MESSAGE)
                    .setPositiveButton(EXIT_WITHOUT_SAVING_YES, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton(EXIT_WITHOUT_SAVING_NO, null)
                    .show();
        } else {
            finish();
        }
    }

    private boolean isNoteChanged()
    {
        String headerText = noteHeader.getText().toString();
        return oldNote == null && headerText != null && !headerText.isEmpty() || oldNote != null &&
                (!oldNote.getHeader().equals(headerText) ||
                !oldNote.getText().equals(noteText.getText().toString()));
    }

    public int getDefaultColor(){
        return Color.parseColor(colors.get(0));
    }
}
