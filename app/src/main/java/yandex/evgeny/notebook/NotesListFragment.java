package yandex.evgeny.notebook;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class NotesListFragment extends Fragment {
    public static final int ADD_NOTE_REQUEST = 0;
    public static final int EDIT_NOTE_REQUEST = 1;
    public static final int RESULT_OK = 0;
    public static final String SAVED_STATE_NOTES_KEY = "notes";

    public static final String INTENT_NOTE_KEY = "note";
    public static final String INTENT_POSITION_KEY = "position";
    public static final String TITLE = "Notebook";
    public static final int defaultPosition = -1;

    private IFragmentsHolder fragmentsHolder;
    private RecyclerNotesAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<Note> notes;



    public static NotesListFragment newInstance(ArrayList<Note> notes) {
        Bundle args = new Bundle();

        NotesListFragment fragment = new NotesListFragment();
        args.putSerializable(SAVED_STATE_NOTES_KEY, notes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notes = (ArrayList) getArguments().getSerializable(SAVED_STATE_NOTES_KEY);
        adapter = new RecyclerNotesAdapter(notes, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notes_list_fragment, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        rootView.findViewById(R.id.add_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNote();
            }
        });

        recyclerView = rootView.findViewById(R.id.notes);
        recyclerView.setLayoutManager(new LinearLayoutManager((Context) fragmentsHolder));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    public void addNote() {
        EditNoteFragment fragment = EditNoteFragment.newInstance(null, defaultPosition);
        fragment.setTargetFragment(this, ADD_NOTE_REQUEST);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void editNote(Note note, int position)
    {
        EditNoteFragment fragment = EditNoteFragment.newInstance(note, position);
        fragment.setTargetFragment(this, EDIT_NOTE_REQUEST);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IFragmentsHolder) {
            fragmentsHolder = (IFragmentsHolder) context;
        } else {
            throw new IllegalStateException("Context must implement IFragmentsHolder");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE_NOTES_KEY, notes);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_NOTE_REQUEST) {
                notes.add((Note)data.getSerializableExtra(INTENT_NOTE_KEY));
            } else if (requestCode == EDIT_NOTE_REQUEST) {
                notes.set(data.getIntExtra(INTENT_POSITION_KEY, defaultPosition),
                        (Note) data.getSerializableExtra(INTENT_NOTE_KEY));
            }
            adapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
