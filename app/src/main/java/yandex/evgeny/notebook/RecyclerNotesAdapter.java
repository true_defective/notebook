package yandex.evgeny.notebook;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;

public class RecyclerNotesAdapter extends RecyclerView.Adapter<RecyclerNotesAdapter.ViewHolder> implements Serializable {
    private static final int borderWidth = 10;
    private ArrayList<Note> notes;
    private NotesListFragment notesListFragment;

    DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

    public RecyclerNotesAdapter(ArrayList<Note> notes, NotesListFragment notesListFragment) {
        this.notes = notes;
        this.notesListFragment = notesListFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.background.setStroke(borderWidth, notes.get(position).getColor());
        holder.tvNoteHeader.setText(notes.get(position).getHeader());
        holder.tvNoteText.setText(notes.get(position).getText());
        holder.tvNoteDate.setText(df.format(notes.get(position).getDate()));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final GradientDrawable background;
        public final TextView tvNoteHeader;
        public final TextView tvNoteText;
        public final TextView tvNoteDate;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notesListFragment.editNote(notes.get(getAdapterPosition()), getAdapterPosition());
                }
            });
            background = (GradientDrawable) view.getBackground();
            tvNoteHeader = view.findViewById(R.id.noteHeader);
            tvNoteText = view.findViewById(R.id.noteText);
            tvNoteDate = view.findViewById(R.id.noteDate);}
    }
}
