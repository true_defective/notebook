package yandex.evgeny.notebook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ColorsEditorFragment extends Fragment {
    private static final String TITLE = "Color editor";
    private static final String INTENT_COLOR_KEY = "color";
    private static final String SAVED_STATE_COLOR_KEY = "color";

    private IFragmentsHolder fragmentsHolder;
    private View viewChosenColor;
    private TextView tvRGBChosenColor;
    private TextView tvHSVChosenColor;
    private LinearLayout colorsHolder;

    private float density;
    private int noteColor;

    private static final int squareSideLen = 64;
    private static final int borderWidth = 3;

    private static ArrayList<String> colors =
            new ArrayList<>(Arrays.asList("#f44242", "#f47a41", "#f4cd41", "#c1f441",
                    "#7ff441", "#41f461", "#41f4a0", "#41f4dc",
                    "#41d0f4", "#4191f4", "#4158f4", "#7f41f4",
                    "#c141f4", "#eb41f4", "#f441af", "#f44161"));
    private int[] intColors;

    public static ColorsEditorFragment newInstance(int noteColor) {

        Bundle args = new Bundle();

        ColorsEditorFragment fragment = new ColorsEditorFragment();
        fragment.noteColor = noteColor;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            noteColor = savedInstanceState.getInt(SAVED_STATE_COLOR_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.color_editing_fragment, container, false);
        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        viewChosenColor = rootView.findViewById(R.id.chosen_color);
        tvRGBChosenColor = rootView.findViewById(R.id.rgb_chosen_color);
        tvHSVChosenColor = rootView.findViewById(R.id.hsv_chosen_color);
        colorsHolder = rootView.findViewById(R.id.colors_holder);

        intColors = new int[colors.size()];
        for (int i = 0; i < colors.size(); i++) {
            intColors[i] = Color.parseColor(colors.get(i));
        }

        density = getResources().getDisplayMetrics().density;
        createViews();
        setColorValue(noteColor);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.color_edit_menu, menu);

        MenuItem colorChooser = menu.findItem(R.id.confirm);
        colorChooser.getIcon().setColorFilter(getResources().getColor(R.color.colorTextIcons), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                saveChoice();
        }

        return true;
    }

    public void saveChoice()
    {
        Fragment fragment = getTargetFragment();
        Intent data = new Intent();
        data.putExtra(EditNoteFragment.INTENT_COLOR_KEY, noteColor);
        fragment.onActivityResult(getTargetRequestCode(), EditNoteFragment.RESULT_OK, data);
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void createViews() {
        int squareSide = (int) (squareSideLen*density);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(squareSide, squareSide);
        layoutParams.setMargins(squareSide/4, squareSide/8,squareSide/4, squareSide/8);

        for (int i = 0; i < intColors.length; i++) {
            View myView = new View(getActivity());
            myView.setLayoutParams(layoutParams);
            MyGradientDrawable background = new MyGradientDrawable();
            background.setColor(intColors[i]);
            background.setStroke(borderWidth, Color.BLACK);
            myView.setBackground(background);

            myView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyGradientDrawable mgd = (MyGradientDrawable) view.getBackground();
                    setColorValue(mgd.GetSavedColor());
                }
            });
            colorsHolder.addView(myView);
        }
        drawGradient();
    }

    private void drawGradient() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);

        int[] gradientColors = new int[intColors.length*2 + 1];
        gradientColors[0] = intColors[0];
        gradientColors[1] = intColors[0];
        for (int i = 1; i < intColors.length; i++) {
            float[] hsvLeft = new float[3];
            Color.colorToHSV(intColors[i-1], hsvLeft);
            float[] hsvRight = new float[3];
            Color.colorToHSV(intColors[i], hsvRight);
            float[] hsvMiddle = new float[] {(hsvLeft[0] + hsvRight[0])/2, (hsvLeft[1] + hsvRight[1])/2, (hsvLeft[2] + hsvRight[2])/2};
            gradientColors[i*2] = Color.HSVToColor(hsvMiddle);
            gradientColors[i*2+1] = intColors[i];
        }
        gradientColors[gradientColors.length - 1] = intColors[intColors.length - 1];

        gradientDrawable.setColors(gradientColors);
        colorsHolder.setBackground(gradientDrawable);
    }

    private void setColorValue(int color) {
        noteColor = color;
        viewChosenColor.setBackgroundColor(noteColor);
        float[] hsv = new float[3];
        Color.colorToHSV(noteColor, hsv);
        tvRGBChosenColor.setText(String.format("#%06X", (0xFFFFFF & noteColor)));
        tvHSVChosenColor.setText(String.format("H:%s S:%s V:%s", (int)hsv[0], (int)hsv[1], (int)hsv[2]));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IFragmentsHolder) {
            fragmentsHolder = (IFragmentsHolder) context;
        } else {
            throw new IllegalStateException("Context must implement IFragmentsHolder");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE_COLOR_KEY, noteColor);
    }
}
