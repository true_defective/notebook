package yandex.evgeny.notebook;

import java.io.Serializable;
import java.util.Date;

public class Note implements Serializable {
    private String header;
    private String text;
    private Date date;
    private int color;

    public Note(String header, String text, int color)
    {
        this.header = header;
        this.text = text;
        date = new Date();
        this.color = color;
    }

    public String getHeader() {
        return header;
    }

    public String getText() {
        return text;
    }

    public Date getDate() {
        return date;
    }

    public int getColor() {
        return color;
    }
}
