package yandex.evgeny.notebook;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IFragmentsHolder {
    private static final String SAVED_STATE_NOTES_KEY = "notes";

    private DrawerLayout mDrawerLayout;
    private ArrayList<Note> notes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

            mDrawerLayout = findViewById(R.id.drawer_layout);
        }


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment fragment;
                        switch (menuItem.getItemId()) {
                            case R.id.main:
                                fragment = NotesListFragment.newInstance(notes);
                                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                break;
                            case R.id.settings:
                                fragment = SettingsFragment.newInstance();
                                break;
                            case R.id.about:
                                fragment = AboutProgramFragment.newInstance();
                                break;
                            default:
                                fragment = NotesListFragment.newInstance(notes);
                        }
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, fragment)
                                .commit();
                        if (mDrawerLayout != null) {
                            mDrawerLayout.closeDrawers();
                        }
                        return true;
                    }
                });
        if (savedInstanceState == null) {
            notes = new ArrayList<>();
            NotesListFragment fragment = NotesListFragment.newInstance(notes);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        } else {
            notes = (ArrayList<Note>) savedInstanceState.getSerializable(SAVED_STATE_NOTES_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE_NOTES_KEY, notes);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        IBackButtonListener listener = (IBackButtonListener) currentFragment;
        if (listener != null)
        {
            listener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem home = menu.findItem(android.R.id.home);
        home.getIcon().setColorFilter(getResources().getColor(R.color.colorTextIcons), PorterDuff.Mode.SRC_ATOP);

        return super.onCreateOptionsMenu(menu);
    }*/
}
